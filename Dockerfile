FROM haskell:8.6.5

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils 2>&1
RUN apt install -y libicu-dev libtinfo-dev libgmp-dev

RUN stack upgrade

RUN git clone https://github.com/haskell/haskell-ide-engine --recurse-submodules
WORKDIR /haskell-ide-engine

RUN stack ./install.hs hie-8.6.5
RUN stack ./install.hs data

WORKDIR /

RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
ENV DEBIAN_FRONTEND=dialog

RUN rm -rf /haskell-ide-engine
