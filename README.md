# docker haskell-ide-engine

Dockerでhieをうごかせるようにするproject  
ローカル環境にdockerが入っていることを想定  
vscodeのremote container extensionを使って開発するサンプルを同梱

## ローカルでのイメージビルド

```sh
$ cd ${YOUR_GIT_CLONED_DIRECTORY}
$ ./build.sh
```

**注意：6GB超の巨大イメージができあがります**

## 動作確認

- vscodeにremote containers extensionをinstall
- remote containersでディレクトリを開き直す
  - https://qiita.com/yoskeoka/items/01c52c069123e0298660 とかの記事を参考に
- .gitディレクトリごとマウントしています。  
コンテナ内でファイルを変更した場合、ホストでgitの変更検知ができなくなることがあります。（とくにLinux）  
うまいことやってください。  
[userns-remap](https://docs.docker.com/engine/security/userns-remap/)を使って解決するのが簡単だと思います。

## 片付け

vscodeを閉じる
```sh
$ docker-compose down
```
